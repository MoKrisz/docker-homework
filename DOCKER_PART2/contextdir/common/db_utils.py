

def get_db_schema(conn):
    # iterate over tables in the database and return the DB schema, such as this example
    #
    # [{'table_name': 'production', 'table_schema': 'categories', 'column_name': 'category_id', 'data_type': 'int'},
    #  {'table_name': 'production', 'table_schema': 'categories', 'column_name': 'category_name', 'data_type': 'varchar'},
    #  {'table_name': 'production', 'table_schema': 'brands', 'column_name': 'brand_id', 'data_type': 'int'},
    #  {'table_name': 'production', 'table_schema': 'brands', 'column_name': 'brand_name', 'data_type': 'varchar'},
    #  {'table_name': 'production', 'table_schema': 'products', 'column_name': 'product_id', 'data_type': 'int'},
    #  {'table_name': 'production', 'table_schema': 'products', 'column_name': 'product_name', 'data_type': 'varchar'},
    #  {'table_name': 'production', 'table_schema': 'products', 'column_name': 'brand_id', 'data_type': 'int'},
    #  {'table_name': 'production', 'table_schema': 'products', 'column_name': 'category_id', 'data_type': 'int'},
    #  {'table_name': 'production', 'table_schema': 'products', 'column_name': 'model_year', 'data_type': 'smallint'},
    #  {'table_name': 'production', 'table_schema': 'products', 'column_name': 'list_price', 'data_type': 'decimal'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'customer_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'first_name', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'last_name', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'phone', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'email', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'street', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'city', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'state', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'customers', 'column_name': 'zip_code', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'stores', 'column_name': 'store_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'stores', 'column_name': 'store_name', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'stores', 'column_name': 'phone', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'stores', 'column_name': 'email', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'stores', 'column_name': 'street', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'stores', 'column_name': 'city', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'stores', 'column_name': 'state', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'stores', 'column_name': 'zip_code', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'staffs', 'column_name': 'staff_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'staffs', 'column_name': 'first_name', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'staffs', 'column_name': 'last_name', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'staffs', 'column_name': 'email', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'staffs', 'column_name': 'phone', 'data_type': 'varchar'},
    #  {'table_name': 'sales', 'table_schema': 'staffs', 'column_name': 'active', 'data_type': 'tinyint'},
    #  {'table_name': 'sales', 'table_schema': 'staffs', 'column_name': 'store_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'staffs', 'column_name': 'manager_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'orders', 'column_name': 'order_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'orders', 'column_name': 'customer_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'orders', 'column_name': 'order_status', 'data_type': 'tinyint'},
    #  {'table_name': 'sales', 'table_schema': 'orders', 'column_name': 'order_date', 'data_type': 'date'},
    #  {'table_name': 'sales', 'table_schema': 'orders', 'column_name': 'required_date', 'data_type': 'date'},
    #  {'table_name': 'sales', 'table_schema': 'orders', 'column_name': 'shipped_date', 'data_type': 'date'},
    #  {'table_name': 'sales', 'table_schema': 'orders', 'column_name': 'store_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'orders', 'column_name': 'staff_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'order_items', 'column_name': 'order_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'order_items', 'column_name': 'item_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'order_items', 'column_name': 'product_id', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'order_items', 'column_name': 'quantity', 'data_type': 'int'},
    #  {'table_name': 'sales', 'table_schema': 'order_items', 'column_name': 'list_price', 'data_type': 'decimal'},
    #  {'table_name': 'sales', 'table_schema': 'order_items', 'column_name': 'discount', 'data_type': 'decimal'},
    #  {'table_name': 'production', 'table_schema': 'stocks', 'column_name': 'store_id', 'data_type': 'int'},
    #  {'table_name': 'production', 'table_schema': 'stocks', 'column_name': 'product_id', 'data_type': 'int'},
    #  {'table_name': 'production', 'table_schema': 'stocks', 'column_name': 'quantity', 'data_type': 'int'}]
    #  ]
    db_schema = []
    script = ["select TABLE_SCHEMA,TABLE_NAME,COLUMN_NAME,DATA_TYPE from INFORMATION_SCHEMA.COLUMNS;"]
    with conn.cursor() as cur:
        for statement in script:
            cur.execute(statement)
        result = cur.fetchall()
    for row in result:
        table_schema = row[0]
        table_name = row[1]
        column_name = row[2]
        data_type = row[3]
        db_schema.append({"table_schema": table_schema,
                          "table_name": table_name,
                          "column_name": column_name,
                          "data_type": data_type })
    return db_schema

def get_qualified_table_names(db_schema):
    qualified_table_names = []
    for row in db_schema:
        q_table_name = f"[{row['table_schema']}].[{row['table_name']}]"
        if q_table_name not in qualified_table_names:
            qualified_table_names.append(q_table_name)
    return  qualified_table_names



