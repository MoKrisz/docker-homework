import datetime
import random
import time
import pytest
import pyodbc
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# the conftest.py contains fixures, hooks and common steps
# the scope of the definitions in the conftest.py file extends to the tests
# files at the same or lower directry levels

@pytest.fixture(autouse=False)
def sql_server_connection_A():
    conn = pyodbc.connect("DRIVER={ODBC Driver 17 for SQL Server};"
                      "SERVER=host.docker.internal;"
                      "DATABASE=test_A;"
                      "UID=arpito;"
                      "PWD=arpito;"
                      "Connection Timeout=60")
    yield conn
    conn.close()

@pytest.fixture(autouse=False)
def sql_server_connection_B():
    conn = pyodbc.connect("DRIVER={ODBC Driver 17 for SQL Server};"
                      "SERVER=host.docker.internal;"
                      "DATABASE=test_B;"
                      "UID=arpito;"
                      "PWD=arpito;"
                      "Connection Timeout=60")
    yield conn
    conn.close()

@pytest.fixture(autouse=False)
def selenium_driver(request):
    root_directory = request.fspath.dirname
    print("\nroot_directory " + root_directory)
    print("\nwebdriver initialized")
    options = webdriver.ChromeOptions()
    #options.add_argument("--headless")
    options.add_argument("--start-maximized")
    capabilities = options.to_capabilities()
    driver = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub', desired_capabilities=capabilities)
    driver.implicitly_wait(10)
    #driver.maximize_window()
    failed_before = request.session.testsfailed
    yield driver
    print("driver yielded")
    if request.session.testsfailed != failed_before:
        test_name = request.node.name
        take_screenshot(driver, root_directory + "/screenshots", test_name)
    # close driver
    #time.sleep(2)
    driver.quit()
    print("webdriver killed")


# this is a util function, just put in conftest for convenience' sake
def take_screenshot(driver, directory_path, test_name ):
    now = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    filepath = f'{directory_path}/{test_name}_%s_screenshot.png' % now

   # alernate non selenium screenshot function commented out but works
   # package pyautogui, note to install Pillow dependency for it first
   #pyautogui.screenshot(filepath)
    driver.save_screenshot(filepath)


# all the step defs  are contained in the conftest file for sharing and separating from test
def navigate_to_google_search(selenium_driver, url):
    print(f"navigating to {url}")
    selenium_driver.get(url)
    search_input = selenium_driver.find_element_by_name('q')
    search_input.send_keys("Arpito" + Keys.RETURN)
    time.sleep(1)
    # generate randomized faiure
    assert random.random() > 0.5, "randomized failure"



