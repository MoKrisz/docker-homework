import pyodbc
from common.db_utils import get_db_schema, get_qualified_table_names


def test_compare_two_db(sql_server_connection_A, sql_server_connection_B ):
    print("\nthis test connects to two local mssql dd-s and compares their content")

    # get the two database scheams
    db_A_schema = get_db_schema(sql_server_connection_A)
    db_B_schema = get_db_schema(sql_server_connection_B)
    print(f"\nDB A Schema ---------------------------------------------------")
    for row in db_A_schema:
        print(row)
    print(f"\nDB B Schema ---------------------------------------------------")
    for row in db_B_schema:
        print(row)

    # assert that two BD scehmas are identical
    assert db_A_schema == db_B_schema

    # extract qualified table names from schemas
    q_table_names_A = get_qualified_table_names(db_A_schema)
    q_table_names_B = get_qualified_table_names(db_B_schema)

    # compute table checksums in both DB-s
    chcksums_A = []
    chcksums_B = []
    for table in q_table_names_A:
        query = f"SELECT CHECKSUM_AGG(CHECKSUM(*)) FROM {table};"
        cursor = sql_server_connection_A.cursor()
        cursor.execute(query)
        chcksum = cursor.fetchall()[0][0]
        chcksums_A.append({"table" : table, "checksum" : chcksum })
        print(f"{table} table checksum = {chcksum}")

    for table in q_table_names_B:
        query = f"SELECT CHECKSUM_AGG(CHECKSUM(*)) FROM {table};"
        cursor = sql_server_connection_B.cursor()
        cursor.execute(query)
        chcksum = cursor.fetchall()[0][0]
        chcksums_B.append({"table" : table, "checksum" : chcksum })
        print(f"{table} table checksum = {chcksum}")

    # assert that table checksums are equal in both DB-s
    assert chcksums_A == chcksums_B