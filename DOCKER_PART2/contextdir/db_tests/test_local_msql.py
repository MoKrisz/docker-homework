import pyodbc
import pytest

def test_connect():
    conn = pyodbc.connect("DRIVER={ODBC Driver 17 for SQL Server};"
                          "SERVER=host.docker.internal;"
                          "DATABASE=test_A;"
                          "UID=arpito;"
                          "PWD=arpito;"
                          "Connection Timeout=60")

    # do a WHERE query on the product table
    query = "SELECT * FROM [test_A].[production].[products] WHERE category_id =6"
    cursor = conn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    #print per row
    for item in result:
      print(item)

    # do a checksum query on the product table
    query = "SELECT CHECKSUM_AGG(CHECKSUM(*)) FROM [test_A].[production].[products]"
    cursor.execute(query)
    result = cursor.fetchall()
    print(f"[test].[production].[products] table checksum = {result[0][0]}")

    #iterate over tables in the database and print their column names and data types
    script = ["USE test_A;", "SELECT Name FROM sys.tables;"]
    with conn.cursor() as cur:
        for statement in script:
            cur.execute(statement)
        result = cur.fetchall()
    for table in result:
       table_name = table[0]
       print(f"TABLE: {table_name} -------------------------------------------")
       query = f"select COLUMN_NAME, DATA_TYPE from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='{table_name}';"
       with conn.cursor() as cur:
         cur.execute(query)
         table_properties = cur.fetchall()
         print(table_properties)

def test_connection_hook(sql_server_connection_A):
    query = "SELECT CHECKSUM_AGG(CHECKSUM(*)) FROM [test_A].[production].[products]"
    cursor = sql_server_connection_A.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    print(f"[test_A].[production].[products] table checksum = {result[0][0]}")
